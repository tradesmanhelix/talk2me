class Talk2me

  @message # Use this parameter to query the user for input, like 'Please give me a file naem:'
  @type # The type of input you're expecting, either 'file' or 'directory'

  def initialize
  end

  # ----------------
  # Function: getInput
  # Description: Displays a prompt for the user to enter a valid file or directory, gets user input, and then checks whether the specified file or directory exists
  # ----------------
  def self.getInput(message, type)

    # Validate that we've received a string message to display to the user
    if message.is_a? String
      @message = message
    else
      puts "'message' parameter needs to be a String, received '#{message.class}' instead"
      return
    end

    # Validate that we've received a string type to check for
    if type.is_a? String
      @type = type
    else
      puts "'type' parameter needs to be a String, received '#{type.class}' instead"
      return
    end

    while true
      ok_continue = false
      err_message = ''

      puts @message
      value = gets.chomp

      if @type == 'directory'
        if !ok_continue = File.directory?(value)
          err_message = "Sorry - #{value} isn't a valid directory. Please try again..."
        end
      elsif @type == 'file'
        if !ok_continue = File.file?(value)
          err_message = "Sorry - #{value} isn't a valid filename. Please try again..."
        end
      else
        puts 'Not specified whether to check for file or directory, so dying gracefully.'
        return
      end

      if ok_continue
        return value # We've been given a valid filename or directory and we're free to keep calm and carry on
      else
        puts err_message
        redo
      end

    end

  end
end

# Use the following line for testing
#puts Talk2me.getInput('Please give me a directory', 'directory')