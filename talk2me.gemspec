Gem::Specification.new do |s|
  s.name        = 'talk2me'
  s.version     = '0.0.0'
  s.date        = '2014-10-30'
  s.summary     = "Easily get and validate file and folder names from your users via the command line."
  s.description = "Getting and validating user input can be a pain, especially when you need your users to give you correct file and folder names. Additionally, we all make a msitake in typing every once in a while ;) This gem aims to make the Ruby/Rails programmer's life easier by handling user input where a file or folder name is expected. USAGE: Talk2me.get_input('message[Prompt message asking the user for a file or directory name]', 'type[file | directory]'). Enjoy!"
  s.authors     = ["Alex Smith"]
  s.email       = 'greatsport1@gmail.com'
  s.files       = ["lib/talk2me.rb"]
  s.homepage    =
    'http://github.com/talk2me'
  s.license       = 'MIT'
end
