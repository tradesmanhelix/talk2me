# GEM: talk2me

Easily get and validate file and folder names from your users via the command line


# DESCRIPTION

Getting and validating user input can be a pain, especially when you need your users to give you correct file and folder names. Additionally, we all make a msitake in typing every once in a while ;) This gem aims to make the Ruby/Rails programmer\'s life easier by handling user input where a file or folder name is expected.


# USAGE

    Talk2me.get_input('message[Prompt message asking the user for a file or directory name]', 'type[file | directory]')
